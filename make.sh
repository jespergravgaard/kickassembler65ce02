#!/bin/bash
# SWITCH TO JAVA VERSION 8
export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
$JAVA_HOME/bin/java -version
## Clear the output directory /out/
[[ -d out ]] && rm -rf out
mkdir out
## Create an empty directory /kickassembler
[[ -d kickassembler ]] && rm -rf kickassembler
mkdir kickassembler
pushd kickassembler > /dev/null
# Download newest KickAssembler to directory /kickassembler/archive
echo Download newest KickAssembler...
[[ -d archive ]] && rm -rf archive
mkdir archive
pushd archive > /dev/null
wget --user-agent="Mozilla/5.0 (Windows NT x.y; rv:10.0) Gecko/20100101 Firefox/10.0" -q http://theweb.dk/KickAssembler/KickAssembler.zip
popd > /dev/null
# Unzip kickassembler to /kickassembler/files
[[ -d files ]] && rm -rf files
mkdir files
unzip -q archive/KickAssembler.zip -d files
# Unpack the kickassembler JAR to /kickassembler/classes
[[ -d classes ]] && rm -rf classes
mkdir classes
unzip -q files/KickAss.jar -d classes
popd > /dev/null
## Delete the output directory /out/classes
[[ -d out/classes ]] && rm -rf out/classes
# Copy all kickassembler classes to /out/classes
echo Compiling and packaging JAR...
cp -R kickassembler/classes out/classes
# Compile all java sources to /out/classes/
find src -name "*.java" > out/sources.txt
$JAVA_HOME/bin/javac -cp kickassembler/files/KickAss.jar -d out/classes @out/sources.txt
# Copy MANIFEST
cp src/META-INF/MANIFEST.MF out/classes/META-INF/
# Create JAR file
$JAVA_HOME/bin/jar cmf out/classes/META-INF/MANIFEST.MF out/KickAss65CE02.jar -C out/classes/ .
# Test the modified KickAssembler
./test.sh