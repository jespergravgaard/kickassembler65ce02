//
// Standard Library Constants and Functions
// for KickAssembler (with added support for 65CE02, HUC6280 & 45GS02)
// Original by Mads Nielsen
// Modified by Jesper Gravgaard
// Source code recreated from a .class file by IntelliJ IDEA (powered by FernFlower decompiler)

package kickass.state.libraries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kickass._65xx._65xxArgType;
import kickass._65xx._65xxArgTypeUtil;
import kickass.common.exceptions.AsmErrorException;
import kickass.parsing.sourcelocation.SourceRange;
import kickass.pass.function.Function;
import kickass.pass.function.special.Function0Arg;
import kickass.pass.function.special.Function1Arg;
import kickass.pass.function.special.Function2Arg;
import kickass.pass.values.HashtableValue;
import kickass.pass.values.ListValue;
import kickass.pass.values.NumberValue;
import kickass.pass.values.OutputFileValue;
import kickass.pass.values.Value;
import kickass.pass.values._65xxArgumentValue;
import kickass.state.EvaluationState;

public class StdConstructorLibrary implements ILibrary {

   public String getName() {
      return "Constructors";
   }

   public List<LibConstant> getConstants() {
      ArrayList<LibConstant> constants = new ArrayList<>();
      constants.add(new LibConstant("AT_ABSOLUTE", new NumberValue(_65xxArgType.unresolvedAbsolute.getIdNo())));
      constants.add(new LibConstant("AT_ZEROPAGE", new NumberValue(_65xxArgType.zeropage.getIdNo())));
      constants.add(new LibConstant("AT_ABSOLUTEX", new NumberValue(_65xxArgType.unresolvedAbsoluteX.getIdNo())));
      constants.add(new LibConstant("AT_ABSOLUTEY", new NumberValue(_65xxArgType.unresolvedAbsoluteY.getIdNo())));
      constants.add(new LibConstant("AT_IMMEDIATE", new NumberValue(_65xxArgType.unresolvedImmediate.getIdNo())));
      constants.add(new LibConstant("AT_INDIRECT", new NumberValue(_65xxArgType.unresolvedIndirect.getIdNo())));
      constants.add(new LibConstant("AT_IZEROPAGEX", new NumberValue(_65xxArgType.unresolvedIndirectX.getIdNo())));
      constants.add(new LibConstant("AT_IZEROPAGEY", new NumberValue(_65xxArgType.indirectZeropageY.getIdNo())));
      constants.add(new LibConstant("AT_NONE", new NumberValue(_65xxArgType.noArgument.getIdNo())));
      return constants;
   }

   public Collection<Function> getFunctions() {
      ArrayList<Function> functions = new ArrayList<>();
      functions.add(new Function("createFile", 1, OutputFileValue.invalid) {
         public Value execute(Value[] values, EvaluationState state, SourceRange range) {
            return new OutputFileValue(values[0].getString(range), state);
         }
      });
      functions.add(new Function0Arg("List", (range) -> {
         return new ListValue(0);
      }));
      functions.add(new Function1Arg("List", ListValue.invalid, (value, range) -> {
         return new ListValue(value.getInt(range));
      }));
      functions.add(new Function0Arg("Hashtable", (range) -> {
         return new HashtableValue();
      }));
      functions.add(new Function2Arg("CmdArgument", (value, value1, range) -> {
         if (value.isInvalid()) {
            throw new AsmErrorException("The type is an invalid value", range);
         } else if (!value1.isInvalid() && !value1.hasDouble()) {
            throw new AsmErrorException("a " + value.getType() + " is not a valid value argument", range);
         } else {
            int valueInt = value.getInt(range);
            _65xxArgType argType = _65xxArgTypeUtil.getArgType(valueInt, range);
            return new _65xxArgumentValue(argType, value1, NumberValue.zero);
         }
      }));
      return functions;
   }
}
