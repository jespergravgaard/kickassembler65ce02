//
// 65XX Parsed Argument
// for KickAssembler (with added support for 65CE02, HUC6280 & 45GS02)
// Original by Mads Nielsen
// Modified by Jesper Gravgaard
// Source code recreated from a .class file by IntelliJ IDEA (powered by FernFlower decompiler)

package kickass._65xx;

import kickass.pass.expressions.expr.ExprNode;

public class _65xxParsedArg {
    public _65xxArgType type;
    public ExprNode expr1;
    public ExprNode expr2;
    public ExprNode expr3;

    public _65xxParsedArg(_65xxArgType type, ExprNode expr1) {
        this.type = type;
        this.expr1 = expr1;
    }

    public _65xxParsedArg(_65xxArgType type, ExprNode expr1, ExprNode expr2) {
        this.type = type;
        this.expr1 = expr1;
        this.expr2 = expr2;
    }

    public _65xxParsedArg(_65xxArgType type, ExprNode expr1, ExprNode expr2, ExprNode expr3) {
        this.type = type;
        this.expr1 = expr1;
        this.expr2 = expr2;
        this.expr3 = expr3;
    }

}

