//
// 65XX Argument Parser
// for KickAssembler (with added support for 65CE02, HUC6280 & 45GS02)
// Original by Mads Nielsen
// Modified by Jesper Gravgaard
// Source code recreated from a .class file by IntelliJ IDEA (powered by FernFlower decompiler)

package kickass._65xx;

import kickass.parsing.baselang.TokenType;
import kickass.parsing.baselang.tokens.IToken;
import kickass.parsing.baselang.tokens.TokenUtil;
import kickass.parsing.baselang.tokenstreams.TokenStreamList;
import kickass.parsing.script.ExprParser;
import kickass.pass.expressions.expr.ExprNode;
import kickass.state.EvaluationState;

public class _65xxArgParser {

    public _65xxArgParser() {
    }

    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    // START: MODIFIED FOR 65CE02/45GS02/HUC6280 SUPPORT

    public static _65xxParsedArg parse(TokenStreamList tokenStreamList, EvaluationState state) {
        int index = tokenStreamList.getIndex();
        IToken token = tokenStreamList.next();
        if (token.isEos()) {
            // NON
            return new _65xxParsedArg(_65xxArgType.noArgument, null);
        } else if (token.getType() == TokenType.Hash) {
            return parseImmediate(tokenStreamList, state);
        } else if (token.getType() == TokenType.ParanthesisRange) {
            return parseIndirect(token, tokenStreamList, state, index);
        } else {
            tokenStreamList.setIndex(index);
            return parseAbsolute(tokenStreamList, state, index);
        }
    }

    private static _65xxParsedArg parseAbsolute(TokenStreamList tokenStreamList, EvaluationState state, int startIdx) {
        IToken comma = tokenStreamList.nextOfType(TokenType.Comma);
        if (comma.isEos()) {
            // ABS (or ZP)
            tokenStreamList.setIndex(startIdx);
            ExprNode expr = ExprParser.parse(tokenStreamList, state);
            return expr == null ? null : new _65xxParsedArg(_65xxArgType.unresolvedAbsolute, expr);
        } else {
            int commaIdx = tokenStreamList.getIndex();
            IToken next = tokenStreamList.next();
            if (next.isIdIgnoreCase("x")) {
                // ABS,X (or ZP,X)
                TokenUtil.addErrorOnExtraTokens(tokenStreamList, state);
                tokenStreamList.setIndexs(startIdx, tokenStreamList.getIndex() - 2);
                ExprNode expr = ExprParser.parse(tokenStreamList, state);
                return expr == null ? null : new _65xxParsedArg(_65xxArgType.unresolvedAbsoluteX, expr);
            } else if (next.isIdIgnoreCase("y")) {
                // ABS,Y (or ZP,Y)
                TokenUtil.addErrorOnExtraTokens(tokenStreamList, state);
                tokenStreamList.setIndexs(startIdx, tokenStreamList.getIndex() - 2);
                ExprNode expr = ExprParser.parse(tokenStreamList, state);
                return expr == null ? null : new _65xxParsedArg(_65xxArgType.unresolvedAbsoluteY, expr);
            } else {
                // ABS,ABS - look for a final comma
                int endIdx = tokenStreamList.getReverseIndex();
                IToken comma2 = tokenStreamList.nextOfType(TokenType.Comma);
                if(comma2.isEos()) {
                    // ZP,REL  - zeropage-relative
                    tokenStreamList.setIndexs(startIdx, commaIdx);
                    ExprNode expr1 = ExprParser.parse(tokenStreamList, state);
                    tokenStreamList.setIndexs(commaIdx, endIdx);
                    ExprNode expr2 = ExprParser.parse(tokenStreamList, state);
                    return expr1 != null && expr2 != null ? new _65xxParsedArg(_65xxArgType.zeropageRelative, expr1, expr2) : null;
                } else {
                    // ABS,ABS,ABS
                    int comma2Idx = tokenStreamList.getIndex();
                    tokenStreamList.setIndexs(startIdx, commaIdx);
                    ExprNode expr1 = ExprParser.parse(tokenStreamList, state);
                    tokenStreamList.setIndexs(commaIdx, comma2Idx);
                    ExprNode expr2 = ExprParser.parse(tokenStreamList, state);
                    tokenStreamList.setIndexs(comma2Idx, endIdx);
                    ExprNode expr3 = ExprParser.parse(tokenStreamList, state);
                    return expr1 != null && expr2 != null&& expr3 != null ? new _65xxParsedArg(_65xxArgType.absoluteAbsoluteAbsolute, expr1, expr2, expr3) : null;

                }
            }
        }
    }

    private static _65xxParsedArg parseImmediate(TokenStreamList tokenStreamList, EvaluationState state) {
        int startIdx = tokenStreamList.getIndex();
        IToken comma = tokenStreamList.nextOfType(TokenType.Comma);
        if (comma.isEos()) {
            // #IMM (or #IMW immediate word)
            tokenStreamList.setIndex(startIdx);
            ExprNode exprNode = ExprParser.parse(tokenStreamList, state);
            return exprNode == null ? null : new _65xxParsedArg(_65xxArgType.unresolvedImmediate, exprNode);
        } else {
            int commaIdx = tokenStreamList.getIndex();
            int endIdx = tokenStreamList.getReverseIndex();
            IToken next2 = tokenStreamList.reverseNextNoWs();
            IToken comma2 = tokenStreamList.reverseNextNoWs();
            boolean isCommaX = comma2.getType() == TokenType.Comma && next2.isIdIgnoreCase("x");
            if (isCommaX) {
                // #IMM,ABS,X (or #IMM,ZP,X )
                tokenStreamList.setIndexs(startIdx, commaIdx);
                ExprNode expr1 = ExprParser.parse(tokenStreamList, state);
                tokenStreamList.setIndexs(commaIdx, endIdx - 2);
                ExprNode expr2 = ExprParser.parse(tokenStreamList, state);
                return expr1 != null && expr2 != null ? new _65xxParsedArg(_65xxArgType.unresolvedImmediateAndAbsoluteX, expr1, expr2) : null;
            } else {
                // #IMM,ABS (or #IMM,ZP )
                tokenStreamList.setIndexs(startIdx, commaIdx);
                ExprNode expr1 = ExprParser.parse(tokenStreamList, state);
                tokenStreamList.setIndexs(commaIdx, endIdx);
                ExprNode expr2 = ExprParser.parse(tokenStreamList, state);
                return expr1 != null && expr2 != null ? new _65xxParsedArg(_65xxArgType.unresolvedImmediateAndAbsolute, expr1, expr2) : null;
            }
        }
    }

    private static _65xxParsedArg parseIndirect(IToken parenthesis, TokenStreamList tokenStreamList, EvaluationState state, int index) {
        IToken comma = tokenStreamList.next();
        IToken next = tokenStreamList.next();

        boolean isCommaY = comma.getType() == TokenType.Comma && next.isIdIgnoreCase("y");
        if (isCommaY) {
            TokenStreamList childStream = parenthesis.getChildStream();
            IToken next2 = childStream.reverseNextNoWs();
            IToken comma2 = childStream.reverseNextNoWs();
            boolean isCommaSP = comma2.getType() == TokenType.Comma && next2.isIdIgnoreCase("sp");
            if (isCommaSP) {
                // ISY (zp,sp),y
                ExprNode expr = ExprParser.parse(childStream, state);
                tokenStreamList.setIndex(index + 3);
                TokenUtil.addErrorOnExtraTokens(tokenStreamList, state);
                return expr == null ? null : new _65xxParsedArg(_65xxArgType.indirectStackZeropageY, expr);
            } else {
                // IND_Y (zp),y
                ExprNode expr = ExprParser.parse(parenthesis.getChildStream(), state);
                TokenUtil.addErrorOnExtraTokens(tokenStreamList, state);
                return expr == null ? null : new _65xxParsedArg(_65xxArgType.indirectZeropageY, expr);
            }
        }

        // Check for Indirect Zeropage Z ($12),Z and 32-bit addressing (($12)),Z
        boolean isCommaZ = comma.getType() == TokenType.Comma && next.isIdIgnoreCase("z");
        if (isCommaZ) {
            // Look for double parenthesis signalling 32-bit
            TokenStreamList childStream = parenthesis.getChildStream();
            final IToken maybeParenthesis2 = childStream.next();
            if (childStream.size() == 1 && maybeParenthesis2.getType() == TokenType.ParanthesisRange) {
                // LIZ ((zp)),z - 32bit indirect Z-indexed
                ExprNode expr = ExprParser.parse(maybeParenthesis2.getChildStream(), state);
                TokenUtil.addErrorOnExtraTokens(tokenStreamList, state);
                return expr == null ? null : new _65xxParsedArg(_65xxArgType.indirect32ZeropageZ, expr);
            } else {
                // IND_Z (zp),z
                ExprNode expr = ExprParser.parse(parenthesis.getChildStream(), state);
                TokenUtil.addErrorOnExtraTokens(tokenStreamList, state);
                return expr == null ? null : new _65xxParsedArg(_65xxArgType.indirectZeropageZ, expr);
            }
        }

        // Check for indirect zeropage X (zp,x)
        TokenStreamList childStream = parenthesis.getChildStream();
        IToken next2 = childStream.reverseNextNoWs();
        IToken comma2 = childStream.reverseNextNoWs();
        boolean isCommaX = comma2.getType() == TokenType.Comma && next2.isIdIgnoreCase("x");
        if (isCommaX) {
            // IND_X (zp,x)
            ExprNode expr = ExprParser.parse(childStream, state);
            tokenStreamList.setIndex(index + 1);
            TokenUtil.addErrorOnExtraTokens(tokenStreamList, state);
            return expr == null ? null : new _65xxParsedArg(_65xxArgType.unresolvedIndirectX, expr);
        }

        // Indirect (ind)
        boolean noComma = comma.isEos();
        if (noComma) {
            final IToken maybeParenthesis2 = childStream.next();
            if (childStream.size() == 1 && maybeParenthesis2.getType() == TokenType.ParanthesisRange) {
                // LIN ((zp)) - 32-bit indirect
                ExprNode expr = ExprParser.parse(maybeParenthesis2.getChildStream(), state);
                TokenUtil.addErrorOnExtraTokens(tokenStreamList, state);
                return expr == null ? null : new _65xxParsedArg(_65xxArgType.indirect32Zeropage, expr);
            } else {
                // IND (abs)  - indirect
                ExprNode expr = ExprParser.parse(parenthesis.getChildStream(), state);
                return expr == null ? null : new _65xxParsedArg(_65xxArgType.unresolvedIndirect, expr);
            }
        }

        // No matching mode
        return null;
    }

    // END: MODIFIED FOR 65CE02/45GS02/HUC6280 SUPPORT
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

}
