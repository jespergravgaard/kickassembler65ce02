//
// 65XX Argument Expression
// for KickAssembler (with added support for 65CE02, HUC6280 & 45GS02)
// Original by Mads Nielsen
// Modified by Jesper Gravgaard
// Source code recreated from a .class file by IntelliJ IDEA (powered by FernFlower decompiler)

package kickass._65xx;

import kickass.common.exceptions.AsmErrorException;
import kickass.pass.expressions.expr.ExprNode;
import kickass.pass.values.NumberValue;
import kickass.pass.values.Value;
import kickass.pass.values._65xxArgumentValue;
import kickass.state.EvaluationState;

// Modifications: Added valueExpr3

public class _65xxArgumentExpr extends ExprNode {
    private static final _65xxArgumentValue noneArgValue;
    public ExprNode valueExpr;
    public ExprNode valueExpr2;
    public ExprNode valueExpr3;
    private _65xxArgType mode;
    private boolean dereference;

    public _65xxArgumentExpr(_65xxArgType type, ExprNode expr1, ExprNode expr2, boolean dereference) {
        this(type, expr1, expr2, null, dereference);
    }

    public _65xxArgumentExpr(_65xxArgType type, ExprNode expr1, ExprNode expr2, ExprNode expr3, boolean dereference) {
        super(expr1 == null ? null : expr1.getSourceRange());
        this.valueExpr = expr1;
        this.valueExpr2 = expr2;
        this.valueExpr3 = expr3;
        this.mode = type;
        this.dereference = dereference;
    }

    public void executePrepass(EvaluationState evaluationState) {
        if (this.valueExpr != null) {
            this.valueExpr.executePrepass(evaluationState);
            if (this.valueExpr2 != null) {
                this.valueExpr2.executePrepass(evaluationState);
                if (this.valueExpr3 != null) {
                    this.valueExpr3.executePrepass(evaluationState);
                }
            }
        }
    }

    public Value evaluate(EvaluationState evaluationState) {
        if (this.mode == _65xxArgType.noArgument) {
            return noneArgValue;
        } else {
            Value value1 = this.valueExpr.evaluate(evaluationState);
            Value value2 = NumberValue.zero;
            if (this.mode.getNoOfArg() > 1) {
                value2 = this.valueExpr2.evaluate(evaluationState);
            }
            Value value3 = NumberValue.zero;
            if (this.mode.getNoOfArg() > 2) {
                value3 = this.valueExpr3.evaluate(evaluationState);
            }

            if (this.dereference && _65xxArgumentValue.has65xxArg(value1)) {
                return value1;
            } else if (!value1.isInvalid() && !value1.hasDouble()) {
                throw new AsmErrorException("The value of a Command Argument Value must be an integer. This expression returns a value of type '" + value1.getType() + "'", this.valueExpr.getSourceRange());
            } else if (this.mode.getNoOfArg() > 1 && !(value2).isInvalid() && !(value2).hasDouble()) {
                throw new AsmErrorException("The value of a Command Argument Value must be an integer. This expression returns a value of type '" + value2.getType() + "'", this.valueExpr2.getSourceRange());
            } else if (this.mode.getNoOfArg() > 2 && !(value3).isInvalid() && !(value3).hasDouble()) {
                throw new AsmErrorException("The value of a Command Argument Value must be an integer. This expression returns a value of type '" + value3.getType() + "'", this.valueExpr3.getSourceRange());
            } else {
                return new _65xxArgumentValue(this.mode, value1, value2, value3);
            }
        }
    }

    static {
        noneArgValue = new _65xxArgumentValue(_65xxArgType.noArgument, NumberValue.zero, NumberValue.zero, NumberValue.zero);
    }
}
