package kickass;

import kickass._65xx.cpus.CPU_45GS02;
import kickass._65xx.cpus.CPU_65CE02;
import kickass._65xx.cpus.CPU_HUC6280;
import kickass._65xx.cpus.Cpu;
import kickass.nonasm.tools.StringUtil;
import kickass.setup.KickAssemblerSetup;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// KickAssembler 65CE02/45GS02 main
// for KickAssembler (with added support for 65CE02 & 45GS02)
// Original by Mads Nielsen
// Modified by Jesper Gravgaard
public class KickAssembler65CE02  {

   public static void main(String[] var0) {
      System.exit(main2(var0));
   }

   public static int main2(String[] var0) {
      addCpu(CPU_65CE02.instance);
      addCpu(CPU_45GS02.instance);
      addCpu(CPU_HUC6280.instance);
      System.out.println("//------------------------------------------------------");
      System.out.println("//" + StringUtil.centerPad(54, "65CE02/45GS02/HUC6280 added by Jesper Gravgaard"));
      return KickAssembler.main2(var0);
   }

   /**
    * Add a CPU to the global CPU collection
    * @param cpu The CPU to add
    */
   private static void addCpu(Cpu cpu) {
      try {
         final Field cpusField = KickAssemblerSetup.class.getDeclaredField("cpus");
         cpusField.setAccessible(true);
         Cpu[] cpus = (Cpu[]) cpusField.get(null);
         final List<Cpu> newCpus = new ArrayList<>(Arrays.asList(cpus));
         newCpus.add(cpu);
         cpusField.set(null, newCpus.toArray(new Cpu[newCpus.size()]));
      } catch(NoSuchFieldException | IllegalAccessException e) {
         System.err.println("Error! Could not add new CPU to KickAssembler "+cpu.name);
         e.printStackTrace();
         System.exit(2);
      }
   }

}
