// Sample program for HUC6280
// Tests the TST instruction

.cpu _huc6280
.pc = $2000
    tst #$5a+2, $55
    tst #1+2,$3+4
    tst #12, $aaaa
    tst #1+2*3,$7654/2
    tst.z #1+2*3,$7654
    tst #$5a+2, $55,x
    tst #1+2,$3+4,x
    tst #12, $aaaa,x
    tst #1+2*3,$7654/2,x
    tst.z #1+2*3,$7654,x
